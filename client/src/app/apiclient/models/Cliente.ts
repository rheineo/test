/* tslint:disable */

declare var Object: any;
export interface ClienteInterface {
  "nombre"?: string;
  "edad"?: number;
  "fechaNacimiento"?: Date;
  "id"?: number;
}

export class Cliente implements ClienteInterface {
  "nombre": string;
  "edad": number;
  "fechaNacimiento": Date;
  "id": number;
  constructor(data?: ClienteInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cliente`.
   */
  public static getModelName() {
    return "Cliente";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cliente for dynamic purposes.
  **/
  public static factory(data: ClienteInterface): Cliente{
    return new Cliente(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cliente',
      plural: 'Clientes',
      path: 'Clientes',
      idName: 'id',
      properties: {
        "nombre": {
          name: 'nombre',
          type: 'string'
        },
        "edad": {
          name: 'edad',
          type: 'number'
        },
        "fechaNacimiento": {
          name: 'fechaNacimiento',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
