import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { Routing } from './app.routing';
import { SDKBrowserModule } from './apiclient/index';
import { AppComponent } from './app.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { MainLayoutComponent } from './layouts/main-layout.component';

import { ClienteModule } from './modules/cliente/cliente.module';


@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    SharedModule,
    Routing,

    SDKBrowserModule.forRoot(),
    SimpleNotificationsModule.forRoot({
      timeOut: 5000,
      showProgressBar: true,
      pauseOnHover: true
    }),

    ClienteModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
