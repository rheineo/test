import { Component } from '@angular/core';
import { LoopBackConfig } from './apiclient/index';
import { environment } from '../environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor() {
    LoopBackConfig.setBaseURL( environment.apiUrl );
    LoopBackConfig.setApiVersion( environment.apiVersion );
  }
}
