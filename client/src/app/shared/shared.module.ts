import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormlyDatePickerComponent } from './../formly/formly-extend.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MyDateRangePickerModule } from 'mydaterangepicker';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyBootstrapModule,
    NgbModule.forRoot(),
    NgxDatatableModule,
    RouterModule,
    CommonModule,
    NgxMyDatePickerModule.forRoot(),
    MyDateRangePickerModule,
    FormlyModule.forRoot({
      types: [{
        name: 'datepicker', component: FormlyDatePickerComponent, extends: 'input'
      }
      ],
    }),
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    FormlyModule,
    FormlyBootstrapModule,
    NgbModule,
    NgxDatatableModule,
    RouterModule
  ],
  providers: [
  ],
  declarations: [
    FormlyDatePickerComponent
  ]
})
export class SharedModule { }
