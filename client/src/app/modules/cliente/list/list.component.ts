import { Component, OnInit } from '@angular/core';
import { Cliente } from './../../../apiclient/models';
import { ClienteApi } from './../../../apiclient/services';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  clientes: Partial<Cliente>[];

  constructor(private clienteApi: ClienteApi) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.clienteApi.find().subscribe(
      data => {
        this.clientes = data;
      },
      error => {

      }
    );
  }

  deleteItem(model) {
    this.clienteApi.deleteById(model.id).subscribe(
      ok => {
        this.getData();
      },
      error => {

      }
    );
  }

}
