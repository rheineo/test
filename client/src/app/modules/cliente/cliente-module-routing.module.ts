import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { MainLayoutComponent } from '../../layouts/main-layout.component';

const routes: Routes = [
  {
    path: 'cliente',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create',
        component: EditComponent
      },
      {
        path: 'edit/:id',
        component: EditComponent
      }
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteModuleRoutingModule { }
