import { Component, OnInit } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { Router, ActivatedRoute } from '@angular/router';

import { Cliente } from './../../../apiclient/models';
import { LoopBackAuth, ClienteApi } from './../../../apiclient/services';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  model: Partial<Cliente>;
  isNewRecord: boolean;

  form = new FormGroup({});

  fields: FormlyFieldConfig[] = [
    {
      className: 'col-4',
      key: 'nombre',
      type: 'input',
      templateOptions: {
        label: 'Nombre',
        placeholder: 'Nombre',
        required: true,
      }
    },
    {
      className: 'col-4',
      key: 'edad',
      type: 'input',
      templateOptions: {
        label: 'Edad',
        type: 'number',
        placeholder: 'Edad',
        required: true,
      }
    },
    {
      className: 'col-4',
      key: 'fechaNacimiento',
      type: 'datepicker',
      templateOptions: {
        label: 'Fecha de Nacimiento:',
        required: true,
      }
    },
  ];

  constructor(
    private _notifications: NotificationsService,
    private loopBackAuth: LoopBackAuth,
    private clienteApi: ClienteApi,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.model = {} as Cliente;
  }

  ngOnInit() {
    this.getModel();
  }

  getModel() {
    this.isNewRecord = true;
    this.model = {} as Cliente;

    this.route.params.subscribe(params => {
      this.model.id = +params['id'];
      if (this.model.id) {

        this.clienteApi.findById(this.model.id).subscribe(
          data => {
            this.model = data;
            this.isNewRecord = false;
          }
        );
      }

    });
  }


  save(model) {
    if (model.fechaNacimiento) {
      model.fechaNacimiento = model.fechaNacimiento.jsdate || model.fechaNacimiento;
    }
    if (this.isNewRecord) {
      this.clienteApi.create(model).subscribe(
        record => {
          this.router.navigate(['cliente']);
          this._notifications.success('Cliente Creado.');
        },
        error => {
          this._notifications.warn('Ooops!');
        }
      );
    } else {
      this.clienteApi.updateAttributes(model.id, model).subscribe(
        record => {
          this.router.navigate(['cliente']);
          this._notifications.success('Cliente Actualizado.');
        },
        error => {
          this._notifications.warn('Ooops!');
        }
      );
    }

  }

}
