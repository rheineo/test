import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteModuleRoutingModule } from './cliente-module-routing.module';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ClienteModuleRoutingModule,
    SharedModule
  ],
  declarations: [ListComponent, EditComponent]
})
export class ClienteModule { }
