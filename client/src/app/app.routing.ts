import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
export const APPROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/cliente'
    }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(APPROUTES, { useHash: true });
