import { NgModule, Component, SimpleChanges } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { IMyOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
  selector: 'app-formly-datepicker',
  styleUrls: ['./formly-datepicker.component.scss'],
  template: `
  <div class="input-container2">
  <input class="form-control"
            placeholder="Date"
            ngx-mydatepicker
            name="mydate"
           [formControl]="formControl"
           [formlyAttributes]="field"
           [options]="myDatePickerOptions"
           locale="en" #dp="ngx-mydatepicker"
           (dateChanged)="onDateChanged($event)"/>
  <i class="fa fa-calendar icon" (click)="dp.toggleCalendar()"></i>
</div>
  `,
})
export class FormlyDatePickerComponent extends FieldType {


  public myDatePickerOptions: IMyOptions = {

    // other options...
    dateFormat: 'mm.dd.yyyy',
  };

  // tslint:disable-next-line:use-life-cycle-interface
  ngDoCheck() {
    if (this.formControl.value) {
      if (!this.formControl.value.jsdate) {
        this.formControl.patchValue({ jsdate: new Date(this.formControl.value)});
      }
    }
  }

  onDateChanged(event: IMyDateModel) {
    // event properties are: event.date, event.jsdate, event.formatted and event.epoc
  }

}
