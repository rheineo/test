var server = require('../server');
var ds = server.dataSources.db;
var lbTables = [
'User', 
'AccessToken', 
'ACL', 
'RoleMapping', 
'Cliente'
];
ds.isActual(lbTables, function (err, actual) {
  if (!actual) {
    ds.autoupdate(lbTables, function (err, result) {
      if (err) throw err;
      console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
      // ds.disconnect();
    });
  }
});
