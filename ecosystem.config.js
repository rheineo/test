module.exports = {
  apps: [{
    name: 'TEST',
    script: 'server/server.js',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],
  deploy: {
    production: { },
    local: {
      "host": ["127.0.0.1"],
      ref: 'origin/master',
      repo: '',
      path: '/var/www/test',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
